module DocSearch.Main where

import qualified Data.Text as T
import System.Environment
import System.IO
import System.Process
import System.Directory
import qualified Data.List as L

import qualified GI.Gtk as GI(init, main)
import qualified Data.GI.Gtk as DGI(applicationNew)

import DocSearch.Interface
import DocSearch.Cache
import DocSearch.Config
import DocSearch.GtkEventHelpers

import Reactive.Banana.Combinators
import Reactive.Banana.Frameworks

import Database.Selda.Backend(runSeldaT, SeldaConnection, seldaConnection)
import Database.Selda.SQLite
import Database.Selda(tryCreateTable)
import Data.Foldable

import Control.Monad.Trans.Resource
import System.FilePath.Lens(directory)
import Control.Lens

import Path

import qualified GI.Gtk as GI
import qualified GI.Gtk.Enums as GIE
import qualified Data.GI.Gtk as DGI
import qualified GI.Gdk as GD
import qualified GI.Gdk.Constants as GDC


data Direction = DirUp | DirDown 

dirFromAction :: Action -> Maybe Direction
dirFromAction Up = Just DirUp
dirFromAction Down = Just DirDown
dirFromAction _ = Nothing

charFromAction :: Action -> Maybe Char
charFromAction (Letter x) = Just x
charFromAction _ = Nothing

appendChar :: Char -> T.Text -> T.Text
appendChar '\b' = T.dropEnd 1
appendChar c = (<>T.pack [c])

labelText :: MonadMoment m => Event Char -> m (Event T.Text)
labelText ev = accumE T.empty (appendChar <$> ev)

eventNetwork :: String -> (Event T.Text -> MomentIO ()) -> AddHandler () -> AddHandler GD.EventKey -> AddHandler [ManualFile] -> (Event T.Text -> MomentIO ()) -> (Event (Future [(Bool, T.Text)]) -> MomentIO ()) -> MomentIO ()
eventNetwork command labSink destroy key sugg searchSink sugSink  = do
  keyEv <- fromAddHandler key
  desEv <- fromAddHandler destroy
  suggEv <- fromAddHandler sugg
  actionEv <- keyEventToAction defaultBindings keyEv
  ltx <- labelText . filterJust . fmap charFromAction $ actionEv
  sugBeh <- stepper [] $ fmap path <$> suggEv
  menuP <- menuPosition suggEv $ filterJust $ fmap dirFromAction actionEv
  searchSink ltx
  labSink ltx
  let chosen = (markChosen <$> menuP)  <*> sugBeh
  changes chosen >>=  sugSink
  spawnProcess $ (getActivePath <$> chosen) <@ filterE (==Accept) actionEv
  reactimate $ GI.mainQuit <$ filterE (==Exit) actionEv
  closeApp desEv
  where
    spawnProcess :: (Event (Maybe T.Text)) -> MomentIO ()
    spawnProcess = reactimate . fmap (\path ->
                                        maybe (return ()) ((>> GI.mainQuit) . createProcess . proc command .(:[]). T.unpack) path) 

markChosen :: (Maybe Int) -> [a] -> [(Bool, a)]
markChosen (Nothing) l = fmap ((,)False) l
markChosen _ [] = []
markChosen (Just 1) (x:xs) = (True, x): fmap ((,)False) xs
markChosen (Just p) (x:xs) = (False, x): markChosen (Just $ p-1) xs

dMain :: SeldaConnection -> String -> ResourceT IO  ()
dMain conn command = do
  _ <- GI.init Nothing
  _ <- DGI.applicationNew Nothing []
  (labsink, destroyEv, keyEv, sugSink) <- liftIO createWindow
  (suggEv, searchSink) <- createAsyncGetter conn
  evNet <- liftIO $ compile $ eventNetwork command  labsink destroyEv keyEv suggEv searchSink sugSink
  liftIO $ actuate evNet
  GI.main
  return ()

getActivePath :: [(Bool, T.Text)] -> Maybe T.Text
getActivePath = fmap snd . L.find fst

menuPosition :: Event [ManualFile] -> Event Direction -> MomentIO (Behavior (Maybe Int))
menuPosition manuals dir = do
  che <- changeEv
  accumB Nothing
    (unionWith (.) (fmap (const $ const Nothing) manuals)  che)
  where
    maxB = stepper 0 (length <$> manuals)
    nextPos max DirUp Nothing = Just max
    nextPos _ DirDown Nothing = Just 1
    nextPos maxv DirDown (Just x) = Just (min maxv $ x+1)
    nextPos _ DirUp (Just x) = Just (max 1 $ x-1)
    changeEv :: MomentIO (Event (Maybe Int -> Maybe Int))
    changeEv = fmap (\mb -> apply (nextPos <$> mb) dir) maxB

main :: IO ()
main = runResourceT $ do
  config <- liftIO getConfig
  let db = sqlitePath config
  liftIO $ createDirectoryIfMissing True $ db^.directory
  (_, conn) <- allocate (sqliteOpen db) seldaClose
  flip runSeldaT conn $ traverse_ indexer  (manualDirectories config)
  runSeldaT (tryCreateTable manuals) conn
  (dMain conn . pdfViewer) config
  
