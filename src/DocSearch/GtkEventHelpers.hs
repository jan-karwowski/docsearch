module DocSearch.GtkEventHelpers where

import Reactive.Banana.Combinators as C
import Reactive.Banana.Frameworks

import qualified GI.Gdk as GD
import qualified GI.Gdk.Constants as GDC

import Data.Map
import GHC.Word(Word32)

data Action = Accept | Exit | Up | Down | Letter Char deriving (Eq, Read, Show)

keyEventToAction :: Map (Word32, [GD.ModifierType])  Action -> Event GD.EventKey -> MomentIO (Event Action)
keyEventToAction bindings = fmap filterJust . mapEventIO keyToAction
  where
    keyToAction evKey = do
      keyval <- GD.getEventKeyKeyval evKey
      modifiers <- GD.getEventKeyState evKey
      char <- GD.keyvalToUnicode keyval >>=
                 (return . toEnum . fromIntegral)
      pure $ Data.Map.lookup (keyval, modifiers) bindings <|> if char /= '\NUL' then Just $ Letter char else Nothing


defaultBindings :: Map (Word32, [GD.ModifierType]) Action
defaultBindings = Data.Map.fromList [
  ((GDC.KEY_Return, []), Accept),
  ((GDC.KEY_KP_Enter, []), Accept),
  ((GDC.KEY_Up, []), Up),
  ((GDC.KEY_Down, []), Down),
  ((GDC.KEY_Escape, []), Exit),
  ((GDC.KEY_g, [GD.ModifierTypeControlMask]), Exit),
  ((GDC.KEY_c, [GD.ModifierTypeControlMask]), Exit),
  ((GDC.KEY_n, [GD.ModifierTypeControlMask]), Down),
  ((GDC.KEY_p, [GD.ModifierTypeControlMask]), Up),
  ((GDC.KEY_Tab, []), Down)
  ]

