module DocSearch.Config where

import Path
import Path.IO
import System.Directory
import System.IO
import Control.Monad
import Options.Applicative
import Control.Applicative

data MainConfig = MainConfig {
  sqlitePath :: FilePath,
  manualDirectories :: Maybe [Path Abs Dir],
  pdfViewer :: String
  } deriving (Show)

data CmdOptions = CmdOptions {
  cacheDbPath :: FilePath,
  directoryPath :: Maybe FilePath,
  pdfViewerCmd :: String
  } deriving Show

cmdParser :: IO (Parser CmdOptions)
cmdParser = do
  defaultDb <- getXdgDirectory XdgCache "docsearch/cache.sqlite"
  defaultDirlist <- getXdgDirectory XdgConfig "docsearch/directories"
  pure $ CmdOptions <$>
    strOption (
      long "cache-db-path" <> short 'b' <>
      metavar "SQLITE_DB" <> help "Cached file database" <>
      value defaultDb
    ) <*>
    optional (strOption (
                 long "directories-list" <> short 'd' <>
                 metavar "DIRECTORIES_LIST" <>
                 help "File containing list of directories to cache" 
    )) <*>
    strOption (long "PDF viewer" <> short 'p' <> metavar "PROGRAM" <> help "PDF viewer command" <> value "xdg-open")

readDirectories :: FilePath -> IO [Path Abs Dir]
readDirectories file = withFile file ReadMode processFile
  where
    processFile h = do
      eof <- hIsEOF h
      if eof
        then return []
        else liftM2 (:) (hGetLine h >>= processLine)  (processFile h)
    processLine :: String -> IO (Path Abs Dir)
    processLine l = parseAbsDir l

getConfig :: IO MainConfig
getConfig = do
  cmdOptions <- cmdParser >>= execParser . flip info (fullDesc <> progDesc "Search indexed pdf manuals by name")
  --  getXdgDirectory XdgCache "docsearch" >>= parseAbsDir >>= createDirIfMissing True
  directories <- traverse readDirectories $ directoryPath cmdOptions
  return $ MainConfig (cacheDbPath cmdOptions) directories (pdfViewerCmd cmdOptions)

