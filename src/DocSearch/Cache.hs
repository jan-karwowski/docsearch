{-# LANGUAGE DeriveGeneric, OverloadedStrings, OverloadedLabels #-}
module DocSearch.Cache where

import Database.Selda
import Database.Selda.Backend
import Control.Concurrent
import Control.Monad
import Control.Monad.Trans.Resource

import Reactive.Banana.Frameworks
import Reactive.Banana.Combinators

import Path.IO
import Path
import qualified Data.Text as T

data ManualFile = ManualFile {
  name :: Text,
  path :: Text
} deriving (Generic, Show)


instance SqlRow ManualFile

manuals :: Table ManualFile
manuals = table "manuals" [#path :- primary]

findByName :: MonadSelda m => Text -> m [ManualFile]
findByName namePart = query $ limit 0 5 $ do
  man <- select manuals
  restrict (man ! #name `like` text ("%" <> namePart <> "%"))
  return man


processFiles :: MonadSelda m => Path Abs Dir -> [Path Abs Dir] -> [Path Abs File] -> m (WalkAction Abs)
processFiles _ _ files = insert manuals ffiles >> return (WalkExclude [])
  where
    ffiles = ((map (\f -> ManualFile (T.pack $ show $ filename f) (T.pack $ toFilePath f)) . filter ((==".pdf").fileExtension)) files)

indexer :: MonadSelda m => [Path Abs Dir] -> m ()
indexer paths = do
  deleteFrom manuals (\r ->  r ! #name .== r ! #name)
  liftIO $ print paths
  mapM_ (walkDir processFiles) paths

createAsyncGetter :: SeldaConnection -> ResourceT IO  (AddHandler [ManualFile], Event Text -> MomentIO ())
createAsyncGetter db = do
  mva <- liftIO $ newEmptyMVar
  (adH, han) <- liftIO $ newAddHandler
  allocate  (forkIO $ forever $ do
    na <- takeMVar mva
    result <- runSeldaT (findByName na) db
    han result
            ) (killThread)
  return $ (adH, reactimate . fmap (asyncQuery mva))

asyncQuery :: MVar Text ->  Text -> IO ()
asyncQuery var na = do
  _ <- tryTakeMVar var
  void $ tryPutMVar var na
