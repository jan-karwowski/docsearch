{-# LANGUAGE OverloadedStrings, OverloadedLabels #-}
module DocSearch.Interface(createWindow,
                           closeApp
                          )where


import qualified Data.Text as T
  
import qualified GI.Gtk as GI
import qualified GI.Gtk.Enums as GIE
import qualified Data.GI.Gtk as DGI
import qualified GI.Gdk as GD

import Control.Event.Handler
import Control.Monad
import Reactive.Banana.Frameworks
import Reactive.Banana.Combinators

prepareMarkup :: [(Bool, T.Text)] -> T.Text
prepareMarkup =
  foldl (\x y -> x<>"\n"<>y) T.empty. map lineMarkup
  where
    lineMarkup (False, x) = x
    lineMarkup (True, x) = "<b>"<>x<>"</b>"

createWindow :: IO (Event T.Text -> MomentIO (), AddHandler (), AddHandler GD.EventKey, Event (Future [(Bool, T.Text)]) -> MomentIO ())
createWindow = do
  win <- GI.windowNew GIE.WindowTypeToplevel
  grid <- GI.gridNew
  lab <- GI.labelNew $ Just ""
  suggestions <- GI.labelNew $ Nothing
  GI.setContainerChild win grid
  GI.gridAttach grid lab 1 1 1 1
  GI.gridAttach grid suggestions 1 2 1 1
  (addhandlerDest, handlerDest) <- newAddHandler
  _ <- GI.onWidgetDestroy win $ handlerDest ()
  (addHandlerKey, handlerKey) <- newAddHandler
  _ <- DGI.onWidgetKeyPressEvent win (handlerKey >=> const (return True))
  GI.widgetShowAll win
  return (reactimate . fmap (GI.labelSetText lab), addhandlerDest, addHandlerKey,
          reactimate' . fmap fmap fmap (GI.labelSetMarkup suggestions . prepareMarkup)
         )


closeApp :: Event a -> MomentIO ()
closeApp = reactimate . fmap (const GI.mainQuit)
